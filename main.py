from typing import List

from fastapi import FastAPI, Depends, HTTPException
from fastapi.responses import FileResponse

from sqlalchemy.orm import Session

from database import SessionLocal, engine, Base
from schemas import EducationS, CvS, GetCv, WorkS, SkillS
import utils

Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/cv/", response_model=CvS)
def create_cv(cv: CvS, db: Session = Depends(get_db)):
    return utils.create_cv(db=db, cv=cv)


@app.get("/cv/{cv_id}/", response_model=GetCv)
def get_cv(cv_id: int, db: Session = Depends(get_db)):
    db_user = utils.get_cv(db, cv_id=cv_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.get("/cv/{cv_id}/generate/")
def create_pdf(cv_id: int, db: Session = Depends(get_db)):
    return FileResponse(utils.create_pdf(db=db, cv_id=cv_id))


@app.get("/cv/", response_model=List[GetCv])
def get_cvs(db: Session = Depends(get_db)):
    return utils.get_cvs(db=db)


@app.post("/cv/{cv_id}/education/", response_model=EducationS)
def create_education_for_cv(cv_id: int, education: EducationS, db: Session = Depends(get_db)):
    return utils.create_cv_education(db=db, education=education, cv_id=cv_id)


@app.post("/cv/{cv_id}/work/", response_model=WorkS)
def create_work_for_cv(cv_id: int, work: WorkS, db: Session = Depends(get_db)):
    return utils.create_cv_work(db=db, work=work, cv_id=cv_id)


@app.post("/cv/{cv_id}/skills/", response_model=SkillS)
def create_skill_for_cv(cv_id: int, skill: SkillS, db: Session = Depends(get_db)):
    return utils.create_cv_skill(db=db, skill=skill, cv_id=cv_id)
