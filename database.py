from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

SQLALCHEMY_DATABASE_URL = "sqlite:///./data.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class Cv(Base):
    __tablename__ = "cvs"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    email = Column(String(255))
    phone = Column(Integer)

    educations = relationship("Education", back_populates="cv")
    works = relationship("Work", back_populates="cv")
    skills = relationship("Skill", back_populates="cv")


class Education(Base):
    __tablename__ = "educations"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    cv_id = Column(Integer, ForeignKey("cvs.id"))

    cv = relationship("Cv", back_populates='educations')


class Work(Base):
    __tablename__ = "works"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    position = Column(String)
    start = Column(Integer)
    end = Column(Integer)
    cv_id = Column(Integer, ForeignKey("cvs.id"))

    cv = relationship("Cv", back_populates="works")


class Skill(Base):
    __tablename__ = "skills"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    cv_id = Column(Integer, ForeignKey("cvs.id"))

    cv = relationship("Cv", back_populates="skills")
