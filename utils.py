from fastapi import HTTPException

from sqlalchemy.orm import Session

from database import Education, Cv, Work, Skill
from schemas import EducationS, CvS, WorkS, SkillS

import pdfkit
import jinja2


def create_pdf(db: Session, cv_id: int):
    cv = db.query(Cv).filter(Cv.id == cv_id).first()
    if cv is None:
        raise HTTPException(status_code=404, detail="Not found")

    path = f'./static/{cv.email}.pdf'

    template_loader = jinja2.FileSystemLoader(searchpath="./")
    template_env = jinja2.Environment(loader=template_loader)
    template_file = "template.html"
    template = template_env.get_template(template_file)
    output = template.render(
        name=cv.name,
        email=cv.email,
        phone=str(cv.phone),
        educations=list(cv.educations),
        works=list(cv.works),
        skills=list(cv.skills)
    )
    file = open(f"./static/{cv.email}.html", 'w')
    file.write(output)
    file.close()
    pdfkit.from_file(f"./static/{cv.email}.html", path)

    return path


def get_cvs(db: Session):
    return db.query(Cv).all()


def get_cv(db: Session, cv_id: int):
    return db.query(Cv).filter(Cv.id == cv_id).first()


def create_cv(db: Session, cv: CvS):
    db_cv = Cv(name=cv.name, email=cv.email, phone=cv.phone)
    db.add(db_cv)
    db.commit()
    db.refresh(db_cv)
    return db_cv


def create_cv_education(db: Session, education: EducationS, cv_id: int):
    db_edu = Education(**education.dict(), cv_id=cv_id)
    db.add(db_edu)
    db.commit()
    db.refresh(db_edu)
    return db_edu


def create_cv_work(db: Session, work: WorkS, cv_id: int):
    db_work = Work(**work.dict(), cv_id=cv_id)
    db.add(db_work)
    db.commit()
    db.refresh(db_work)
    return db_work


def create_cv_skill(db: Session, skill: SkillS, cv_id: int):
    db_skill = Skill(**skill.dict(), cv_id=cv_id)
    db.add(db_skill)
    db.commit()
    db.refresh(db_skill)
    return db_skill
