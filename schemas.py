from typing import List

from pydantic import BaseModel


class EducationS(BaseModel):
    name: str
    start: int
    end: int

    class Config:
        orm_mode = True


class WorkS(BaseModel):
    name: str
    position: str
    start: int
    end: int

    class Config:
        orm_mode = True


class SkillS(BaseModel):
    name: str

    class Config:
        orm_mode = True


class CvS(BaseModel):
    name: str
    email: str
    phone: int

    class Config:
        orm_mode = True


class GetCv(CvS):
    id: int
    educations: List[EducationS] = []
    works: List[WorkS] = []
    skills: List[SkillS] = []

    class Config:
        orm_mode = True
